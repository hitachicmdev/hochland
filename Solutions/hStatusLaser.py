import socket
import sys

def GetDataBytes(response):
  return response.split()[4:]

def GetDataBytesEx(response):
  return response.split()[6:]

def Reverse(listIn):
  return listIn[::-1]

def ToStr(listHex):
  fullString = ""
  for item in listHex:
    if len(item) == 1:
      item = '0' + item
    fullString = fullString + item
  return fullString.upper()

def ToInt(listHex):
  return int(ToStr(listHex), 16)

def MaskMatch(mask, flag):
  return (int(mask, 16) & int(flag, 16)) == int(flag, 16)

def AddItem(mask, item):
  if mask == "":
    return item
  else:
    return mask + ", " + item

def UpdateAlarmStatus(alarmStatus):
	if alarmStatus == 0:
		alarmStatus = 'No alarms.'
	elif alarmStatus == 3086:
		alarmStatus = 'Wrong messageport.'
	elif alarmStatus == 2120:
		alarmStatus = 'Some alarms are active.'
	elif alarmStatus == 65535:
		alarmStatus = 'Initialization of Firmware SL has failed due to some hardware failure.'
	return alarmStatus

def UpdatePrintMode(printMode):
	if printMode == '0':
		result = 'System is not in the printing mode.'
	elif printMode == '1':
		result = 'System is in printing mode, but is actually not printing.'
	elif printMode == '2':
		result = 'System has received the STOP signal during printing and breaks the printing process.'
	elif printMode == '3':
		result = 'System is in printing mode and is actually printing.'
	return result

def UpdatePrintModeShort(printMode):
	if printMode == '0':
		result = 'Offline but ready'
	elif printMode == '1':
		result = 'In printing mode'
	elif printMode == '2':
		result = 'Received STOP'
	elif printMode == '3':
		result = 'Printing'
	return result

def UpdateStatusG(printMode):
	if printMode == '0':
		result = '0'
	elif printMode == '1':
		result = '1'
	elif printMode == '2':
		result = '0'
	elif printMode == '3':
		result = '1'
	return result

def UpdateStatusR(printMode):
	if UpdateStatusG(printMode) == '0':
		result = '1'
	else:
		result = '0'
	return result

def UpdateAlarmCode(alarmCode):
	if alarmCode == '0000':
		alarmCode = ''
	elif alarmCode == '0002':
		alarmCode = 'Laser is OFF (interlock open).'
	elif alarmCode == '000A':
		alarmCode = 'Belt stopped.'
	elif alarmCode == '000C':
		alarmCode = 'Wrong figure-type in file.'
	elif alarmCode == '000D':
		alarmCode = 'No memory available.'
	elif alarmCode == '0010':
		alarmCode = 'File not found.'
	elif alarmCode == '0015':
		alarmCode = 'Invalid font (or not existing).'
	elif alarmCode == '0016':
		alarmCode = 'Overtemperature.'
	elif alarmCode == '0025':
		alarmCode = 'Shutter closed.'
	elif alarmCode == '0026':
		alarmCode = 'Laser not ready.'
	elif alarmCode == '0027':
		alarmCode = 'OEM-shutter closed.'
	elif alarmCode == '0028':
		alarmCode = 'PowerOn alarm.'
	elif alarmCode == '0030':
		alarmCode = 'Overspeed.'
	elif alarmCode == '0031':
		alarmCode = 'Harddisk full.'
	elif alarmCode == '0032':
		alarmCode = 'File not allowed to print.'
	elif alarmCode == '0033':
		alarmCode = 'Barcode creation failed (wrong parameters).'
	elif alarmCode == '0034':
		alarmCode = 'No barcode license.'
	elif alarmCode == '0035':
		alarmCode = 'No barcode library (wrong version of Firmware SL).'
	elif alarmCode == '0036':
		alarmCode = 'Multiple trigger signal during printing.'
	elif alarmCode == '0037':
		alarmCode = 'Database not available.'
	elif alarmCode == '0038':
		alarmCode = 'Maximum distance between prints reached.'
	elif alarmCode == '0039':
		alarmCode = 'Minimum distance between prints not reached.'
	elif alarmCode == '0040':
		alarmCode = 'Client timeout.'
	elif alarmCode == '0041':
		alarmCode = 'X-scanner not ready.'
	elif alarmCode == '0042':
		alarmCode = 'Y-scanner not ready.'
	elif alarmCode == '0043':
		alarmCode = 'Message is empty.'
	elif alarmCode == '0044':
		alarmCode = 'Initialization failed (requires a restart).'
	return alarmCode

def UpdateAlarmMask(mask):
	alarmMask = ""

	if MaskMatch(mask, "1"):
		alarmMask = AddItem(alarmMask, "Interlock")
	if MaskMatch(mask, "2"):
		alarmMask = AddItem(alarmMask, "OEM-shutter")
	if MaskMatch(mask, "4"):
		alarmMask = AddItem(alarmMask, "Overtemperature")
	if MaskMatch(mask, "8"):
		alarmMask = AddItem(alarmMask, "Shutter")
	if MaskMatch(mask, "10"):
		alarmMask = AddItem(alarmMask, "Laser not ready")
	if MaskMatch(mask, "20"):
		alarmMask = AddItem(alarmMask, "X-scanner failure")
	if MaskMatch(mask, "40"):
		alarmMask = AddItem(alarmMask, "Y-scanner failure")
	if MaskMatch(mask, "80"):
		alarmMask = AddItem(alarmMask, "power failure")
	if MaskMatch(mask, "100"):
		alarmMask = AddItem(alarmMask, "reserved")
	if MaskMatch(mask, "200"):
		alarmMask = AddItem(alarmMask, "Laser not armed")
	if MaskMatch(mask, "400"):
		alarmMask = AddItem(alarmMask, "reserved")
	if MaskMatch(mask, "800"):
		alarmMask = AddItem(alarmMask, "Q-switch")
	if MaskMatch(mask, "1000"):
		alarmMask = AddItem(alarmMask, "trigger signal")
	if MaskMatch(mask, "2000"):
		alarmMask = AddItem(alarmMask, "file not allowed (wrong version)")
	if MaskMatch(mask, "4000"):
		alarmMask = AddItem(alarmMask, "overspeed")
	if MaskMatch(mask, "8000"):
		alarmMask = AddItem(alarmMask, "harddisk full")
	if MaskMatch(mask, "10000"):
		alarmMask = AddItem(alarmMask, "barcode creation failure")
	if MaskMatch(mask, "20000"):
		alarmMask = AddItem(alarmMask, "barcode licence failure")
	if MaskMatch(mask, "40000"):
		alarmMask = AddItem(alarmMask, "barcode library failure")
	if MaskMatch(mask, "80000"):
		alarmMask = AddItem(alarmMask, "invalid file")
	if MaskMatch(mask, "100000"):
		alarmMask = AddItem(alarmMask, "database failure")
	if MaskMatch(mask, "200000"):
		alarmMask = AddItem(alarmMask, "max.-distance alarm")
	if MaskMatch(mask, "400000"):
		alarmMask = AddItem(alarmMask, "min.-distance alarm")
	if MaskMatch(mask, "800000"):
		alarmMask = AddItem(alarmMask, "client-timeout (tcpip)")
	if MaskMatch(mask, "1000000"):
		alarmMask = AddItem(alarmMask, "invalid font")
	if MaskMatch(mask, "2000000"):
		alarmMask = AddItem(alarmMask, "belt stopped")
	if MaskMatch(mask, "4000000"):
		alarmMask = AddItem(alarmMask, "empty message")
	if MaskMatch(mask, "8000000"):
		alarmMask = AddItem(alarmMask, "initialization error")
	if MaskMatch(mask, "10000000"):
		alarmMask = AddItem(alarmMask, "memory error")
	if MaskMatch(mask, "20000000"):
		alarmMask = AddItem(alarmMask, "warmup in progress")
	if MaskMatch(mask, "40000000"):
		alarmMask = AddItem(alarmMask, "OEM alarm active")
	if MaskMatch(mask, "80000000"):
		alarmMask = AddItem(alarmMask, "extended alarm active")
	return alarmMask

def CollectStatus(printerData, printerDataEx, printerSpeed, printerPower):
	data = GetDataBytes(printerData)
	result = {}
	result["printed"] = str(ToInt(Reverse(data[:4])))
	result["printedAll"] = str(ToInt(Reverse(data[16:20])))
	result["alarmStatus"] = UpdateAlarmStatus(ToInt(Reverse(data[24:26])))
	result["statusG"] = UpdateStatusG(data[15])
	result["statusR"] = UpdateStatusR(data[15])
	result["printingMode"] = UpdatePrintMode(data[15])
	result["printingModeShort"] = UpdatePrintModeShort(data[15])
	result["alarmCode"] = UpdateAlarmCode(ToStr(Reverse(data[26:28])))
	result["printTime"] = str(ToInt(Reverse(data[28:32])))
	result["alarmMask"] = UpdateAlarmMask(ToStr(Reverse(data[40:44])))
	result["fileName"] = ToStr(GetDataBytesEx(printerDataEx)[40:56]).decode("hex").rstrip('\0')
	result["speed"] =  str(ToInt(Reverse(GetDataBytes(printerSpeed)[8:12])))
	result["power"] = str(ToInt(Reverse(GetDataBytes(printerPower)[8:12])))
	return result

def IsDeviceOnline(ipAddress, port):
	BUFFER_SIZE = 1024
	STX = chr(0x02)
	ETX = chr(0x03)
	TIMEOUT = 2.0
	result = {}

	try:
		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		s.settimeout (TIMEOUT)
		s.connect((ipAddress, int(port)))
		data = s.recv(BUFFER_SIZE)
		return True
	except:
		return False
	finally:
		s.close()

def IsDeviceOnlinePing(ipAddress, port):
	import os
	response = os.system("ping -n 1 " + ipAddress + " > nul")
	if response == 0:
		pingstatus = True
	else:
		pingstatus = False
	return pingstatus

def GetStatus(ipAddress, port):
	BUFFER_SIZE = 1024
	STX = chr(0x02)
	ETX = chr(0x03)
	TIMEOUT = 2.0
	result = {}

	if IsDeviceOnline(ipAddress, port):
		try:
			s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			s.settimeout (TIMEOUT)
			s.connect((ipAddress, int(port)))
			data = s.recv(BUFFER_SIZE)
			s.send(STX + chr(0x02) + chr(0x70) + chr(0x00) + ETX)
			data1 = s.recv(BUFFER_SIZE)
			s.send(STX + chr(0x04) + chr(0x70) + chr(0x01) + chr(0x00) + chr(0x00) + ETX)
			data2 = s.recv(BUFFER_SIZE)
			s.send(STX + chr(0x0E) + chr(0x76) + chr(0x00) + chr(0x01) + chr(0x00) + chr(0x00) + chr(0x00) + chr(0x02) + chr(0x00) + chr(0x00) + chr(0x00) + chr(0x00) + chr(0x00) + chr(0x00) + chr(0x00) + ETX)
			speed = s.recv(BUFFER_SIZE)
			s.send(STX + chr(0x0E) + chr(0x76) + chr(0x00) + chr(0x01) + chr(0x00) + chr(0x00) + chr(0x00) + chr(0x03) + chr(0x00) + chr(0x00) + chr(0x00) + chr(0x00) + chr(0x00) + chr(0x00) + chr(0x00) + ETX)
			power = s.recv(BUFFER_SIZE)
			printerStatus = ' '.join(format(ord(x), 'x') for x in data)
			printerData = ' '.join(format(ord(x), 'x') for x in data1)
			printerDataEx = ' '.join(format(ord(x), 'x') for x in data2)
			printerSpeed = ' '.join(format(ord(x), 'x') for x in speed)
			printerPower = ' '.join(format(ord(x), 'x') for x in power)
		except:
			result["printed"] = ""
			result["printedAll"] = ""
			result["alarmStatus"] = "\r\n".join(map(str, sys.exc_info()))
			result["statusG"] = ""
			result["statusR"] = ""
			result["printingMode"] = "Offline"
			result["printingModeShort"] = "Offline"
			result["alarmCode"] = ""
			result["printTime"] = ""
			result["alarmMask"] = ""
			result["fileName"] = ""
			result["speed"] = ""
			result["power"] = ""
			printerSpeed = ""
			printerPower = ""
			printerStatus = ""
			printerData = ""
			return result
		finally:
			s.close()
	else:
		result["printed"] = ""
		result["printedAll"] = ""
		result["alarmStatus"] = sys.exc_info()[1]
		result["statusG"] = "0"
		result["statusR"] = ""
		result["printingMode"] = "Offline"
		result["printingModeShort"] = "Offline"
		result["alarmCode"] = ""
		result["printTime"] = ""
		result["alarmMask"] = ""
		result["fileName"] = ""
		result["speed"] = ""
		result["power"] = ""
		printerSpeed = ""
		printerPower = ""
		printerStatus = ""
		printerData = ""
		return result		

	return CollectStatus(printerData, printerDataEx, printerSpeed, printerPower)

def GetStatusTest(ipAddress, port):
	printerData = "2 32 70 0 0 0 0 0 0 0 0 0 ff ff 0 0 0 0 0 0 63 d0 2 0 1 0 0 0 48 8 25 0 0 0 0 0 64 65 67 72 65 65 73 20 9 0 0 0 0 0 0 0 3"
	printerDataEx = "2 4 70 1 48 0 0 0 0 5 0 0 0 5 0 0 ff ff 0 0 0 0 0 2 d0 68 0 0 0 0 0 25 8 48 0 0 19 6d 0 0 0 8 0 2 0 0 30 34 20 2d 20 69 6e 74 65 72 6e 61 2e 78 6d 6c 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 3"

	return CollectStatus(printerData, printerDataEx)

def TriggerPrint(ipAddress, port):
	"""Sends command to the printer to print a label"""
	BUFFER_SIZE = 1024
	STX = chr(0x02)
	ETX = chr(0x03)
	TIMEOUT = 2.0
	result = {}

	try:
		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		s.settimeout (TIMEOUT)
		s.connect((ipAddress, int(port)))
		data = s.recv(BUFFER_SIZE)
		s.send(STX + chr(0x02) + chr(0x56) + chr(0x00) + ETX)
		data1 = s.recv(BUFFER_SIZE)
		return True
	except:
		return False
	finally:
		s.close()


