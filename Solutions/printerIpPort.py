import win32com.client

def PrinterPort(printer):
	"""Returns the port name to which the printer is connected to"""
	strComputer = "."
	objWMIService = win32com.client.Dispatch("WbemScripting.SWbemLocator")
	objSWbemServices = objWMIService.ConnectServer(strComputer,"root\cimv2")
	colItems = objSWbemServices.ExecQuery("SELECT * FROM Win32_Printer")
	for objItem in colItems:
		if objItem.Name == printer:
			return objItem.portName
			break

def IpPortFromName(portName):
	"""Returns the IP address and port number for the network connected printer"""
	result = ''
	strComputer = "."
	objWMIService = win32com.client.Dispatch("WbemScripting.SWbemLocator")
	objSWbemServices = objWMIService.ConnectServer(strComputer,"root\cimv2")
	colItems = objSWbemServices.ExecQuery("SELECT * FROM Win32_TCPIPPrinterPort")
	for objItem in colItems:
		if portName == objItem.Name:
			result = objItem.HostAddress
			break
	if result == '':
		import _winreg
		HKEY_LOCAL_MACHINE = 2147483650
		aReg = _winreg.ConnectRegistry(None,HKEY_LOCAL_MACHINE)
		aKey = _winreg.OpenKey(aReg, r"SYSTEM\CurrentControlSet\Control\Print\Monitors\Advanced Port Monitor\Ports")
		for i in range(1024):
			try:
				asubkey_name = _winreg.EnumKey(aKey,i)
				if asubkey_name == portName:
					asubkey = _winreg.OpenKeyEx(aKey,asubkey_name)
					address = _winreg.QueryValueEx(asubkey, "IPAddress")
					port = _winreg.QueryValueEx(asubkey, "PortNumber")
					result = address[0], port[0]
			except EnvironmentError:
				break
	return result

if __name__ == "__main__":
	print IpPortFromName(PrinterPort('Hitachi UX-D160W MODBUS'))
	print IpPortFromName(PrinterPort('LM-C310P-100B5N'))

