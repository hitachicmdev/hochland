﻿import socket
import struct
import sys
import time

def GetDataBytes(response):
    """Response is in last byte"""
    return response.split()[-2:]

def UpdateOperationStatus(status):
    """Returns decription of the status code. See UX_SOP-04_Ver.1.pdf"""
    if status == '30':
        result = 'Stop'
    elif status == '31':
        result = 'Standby, Cover, Drop adjust, Ink heating'
    elif status == '32':
        result = 'Ready'
    elif status == '49':
        result = 'Stopping'
    elif status == '33':
        result = 'Deflection Voltage Fault'
    elif status == '34':
        result = 'Main Ink Tank Too Full'
    elif status == '35':
        result = 'Blank Print Items'
    elif status == '36':
        result = 'Ink Drop Charge Too Low'
    elif status == '37':
        result = 'Ink Drop Charge Too High'
    elif status == '38':
        result = 'Print Head Cover Open'
    elif status == '39':
        result = 'Target Sensor Fault'
    elif status == '3A':
        result = 'SYSTEM OPERATION ERROR C'
    elif status == '3B':
        result = 'Target Spacing Too Close'
    elif status == '3C':
        result = 'Improper Sensor Position'
    elif status == '3D':
        result = 'SYSTEM OPERATION ERROR M'
    elif status == '3E':
        result = 'Charge Voltage Fault'
    elif status == '3F':
        result = 'Barcode Short On Numbers'
    elif status == '41':
        result = 'Multi DC Power Supply Fan Fault'
    elif status == '42':
        result = 'Deflection Voltage Leakage'
    elif status == '43':
        result = 'Print Overlap Fault'
    elif status == '44':
        result = 'Ink Low Fault'
    elif status == '45':
        result = 'Makeup Ink Low Fault'
    elif status == '46':
        result = 'Print Data Changeover In Progress M'
    elif status == '47':
        result = 'Excessive Format Count'
    elif status == '48':
        result = 'Makeup Ink Replenishment Time-out'
    elif status == '4A':
        result = 'Ink Replenishment Time-out'
    elif status == '4B':
        result = 'No Ink Drop Charge'
    elif status == '4C':
        result = 'Ink Heating Unit Too High'
    elif status == '4D':
        result = 'Ink Heating Unit Temperature Sensor Fault'
    elif status == '4E':
        result = 'Ink Heating Unit Over Current'
    elif status == '4F':
        result = 'Internal Communication Error C'
    elif status == '50':
        result = 'Internal Communication Error M'
    elif status == '51':
        result = 'Internal Communication Error S'
    elif status == '52':
        result = 'SYSTEM OPERATION ERROR S'
    elif status == '53':
        result = 'Memory Fault C'
    elif status == '54':
        result = 'Memory Fault M'
    elif status == '55':
        result = 'Ambient Temperature Sensor Fault'
    elif status == '56':
        result = 'Print Controller Cooling Fan Fault'
    elif status == '59':
        result = 'Print Data Changeover In Progress S'
    elif status == '5A':
        result = 'Print Data Changeover In Progress V'
    elif status == '5D':
        result = 'Memory Fault S'
    elif status == '5E':
        result = 'Pump Motor Fault'
    elif status == '5F':
        result = 'Viscometer Ink Temperature Sensor Fault'
    elif status == '60':
        result = 'External Communication Error'
    elif status == '61':
        result = 'External Signal Error'
    elif status == '62':
        result = 'Memory Fault OP'
    elif status == '63':
        result = 'Ink Heating Unit Temperature Low'
    elif status == '64':
        result = 'Model-key Fault'
    elif status == '65':
        result = 'Language-key Fault'
    elif status == '66':
        result = 'Communication Buffer Fault'
    elif status == '67':
        result = 'Shutdown Fault'
    elif status == '68':
        result = 'Count Overflow'
    elif status == '69':
        result = 'Data changeover timing fault'
    elif status == '6A':
        result = 'Count changeover timing fault'
    elif status == '6B':
        result = 'Print start timing fault'
    elif status == '6C':
        result = 'Ink Shelf Life Information'
    elif status == '6D':
        result = 'Makeup Shelf Life Information'
    elif status == '71':
        result = 'Print Data Changeover Error C'
    elif status == '72':
        result = 'Print Data Changeover Error M'    
    else:
        result = status
    return result

def UpdateWarningStatus(status):
    """Returns decription of the status code. See UX_SOP-04_Ver.1.pdf"""
    if status == '30':
        result = 'No warning'
    elif status == '31':
        result = 'Ink Low Warning'
    elif status == '32':
        result = 'Makeup Ink Low Warning'
    elif status == '33':
        result = 'Ink Shelf Life Exceeded'
    elif status == '34':
        result = 'Battery Low M'
    elif status == '35':
        result = 'Ink Pressure High'
    elif status == '36':
        result = 'Product Speed Matching Error'
    elif status == '37':
        result = 'External Communication Error nnn'
    elif status == '38':
        result = 'Ambient Temperature Too High'
    elif status == '39':
        result = 'Ambient Temperature Too Low'
    elif status == '3B':
        result = 'External Signal Error nnn'
    elif status == '3C':
        result = 'Ink Pressure Low'
    elif status == '3D':
        result = 'Excitation V-ref. Review'
    elif status == '3E':
        result = 'Viscosity Reading Instability'
    elif status == '3F':
        result = 'Viscosity Readings Out of Range'
    elif status == '40':
        result = 'High Ink Viscosity'
    elif status == '41':
        result = 'Low Ink Viscosity'
    elif status == '42':
        result = 'Excitation V-ref. Review 2'
    elif status == '44':
        result = 'Battery Low C'
    elif status == '45':
        result = 'Calendar Content Inaccurate'
    elif status == '46':
        result = 'Excitation V-ref. Char. height Review'
    elif status == '49':
        result = 'Model-key Failure'
    elif status == '4A':
        result = 'Language-key Failure'
    elif status == '4C':
        result = 'Upgrade-key Fault'
    elif status == '50':
        result = 'Circulation System Cooling Fan Fault'
    elif status == '51':
        result = 'Ink Temperature Too High'
    else:
        result = status
    return result

def CollectStatus(data):
    """Returns dictionary with the status key-value pairs"""
    result = {}
    result["operationStatus"] = UpdateOperationStatus(data["operationStatus"][1])
    result["warningStatus"] = UpdateWarningStatus(data["warningStatus"][1])
    result["typeName"] = data["typeName"]
    result["inkName"] = data["inkName"]
    result["inkOperatingTime"] = data["inkOperatingTime"]
    result["inkAlarmTime"] = data["inkAlarmTime"]
    result["printCount"] = data["printCount"]
    result["cumulativeOperationTime"] = data["cumulativeOperationTime"]
    result["inkMakeupInkType"] = data["inkMakeupInkType"]
    result["inkViscosity"] = data["inkViscosity"]
    result["inkPressure"] = data["inkPressure"]
    result["ambientTemperature"] = data["ambientTemperature"]
    result["deflectionVoltage"] = data["deflectionVoltage"]
    result["faultWarningMessageCount"] = data["faultWarningMessageCount"]
    return result

def IsDeviceOnline(ipAddress, port):
    """Returns Boolean response whether the device is online"""
    BUFFER_SIZE = 1024
    TIMEOUT = 2.0
    statusCodes = {}
    data = ''

    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.settimeout (TIMEOUT)
        s.connect((ipAddress, int(port)))
        unitId = 01
        functionCode = 04

        req = struct.pack('12B', 0x00, 0x00, 0x00, 0x00, 0x00, 0x06, int(unitId), int(functionCode), 0x00, 0x02, 0x00, 0x01)
        s.send(req)
        data = s.recv(BUFFER_SIZE)
        time.sleep(0.05)
        return True
    except:
        return False
    finally:
        s.close()

def IsDeviceOnlinePing(ipAddress, port):
    import os
    response = os.system("ping -n 1 " + ipAddress + " > nul")
    if response == 0:
        pingstatus = True
    else:
        pingstatus = False
    return pingstatus

def GetStatus(ipAddress, port):
    # Create a TCP/IP socket
    BUFFER_SIZE = 1024
    TIMEOUT = 2.0
    statusCodes = {}
    data = ''
    # Meaning of 'decode' field:
    # 0 - Convert received data into string, remove NULL characters and trim spaces
    # 1 - take the values (2 bytes) and look them up in separate functions (only 2nd byte is used anyway)
    # 2 - Convert the received data into integer
    queryList = {
    'operationStatus':          {'addr1': '0x00','addr2': '0x02', 'range1': '0x00', 'range2': '0x01', 'decode': '1'},
    'warningStatus':            {'addr1': '0x00','addr2': '0x03', 'range1': '0x00', 'range2': '0x01', 'decode': '1'},
    'typeName':                 {'addr1': '0x00','addr2': '0x10', 'range1': '0x00', 'range2': '0x0F', 'decode': '0'},
    'inkName':                  {'addr1': '0x00','addr2': '0x22', 'range1': '0x00', 'range2': '0x0A', 'decode': '0'},
    'inkOperatingTime':         {'addr1': '0x00','addr2': '0x50', 'range1': '0x00', 'range2': '0x01', 'decode': '2'},
    'inkAlarmTime':             {'addr1': '0x00','addr2': '0x51', 'range1': '0x00', 'range2': '0x01', 'decode': '2'},
    'printCount':               {'addr1': '0x00','addr2': '0x52', 'range1': '0x00', 'range2': '0x02', 'decode': '2'},
    'cumulativeOperationTime':  {'addr1': '0x00','addr2': '0x54', 'range1': '0x00', 'range2': '0x02', 'decode': '2'},
    'inkMakeupInkType':         {'addr1': '0x00','addr2': '0x56', 'range1': '0x00', 'range2': '0x01', 'decode': '2'},
    'inkViscosity':             {'addr1': '0x00','addr2': '0x57', 'range1': '0x00', 'range2': '0x01', 'decode': '2'},
    'inkPressure':              {'addr1': '0x00','addr2': '0x58', 'range1': '0x00', 'range2': '0x01', 'decode': '2'},
    'ambientTemperature':       {'addr1': '0x00','addr2': '0x59', 'range1': '0x00', 'range2': '0x01', 'decode': '2'},
    'deflectionVoltage':        {'addr1': '0x00','addr2': '0x5A', 'range1': '0x00', 'range2': '0x01', 'decode': '2'},
    'faultWarningMessageCount': {'addr1': '0x00','addr2': '0x70', 'range1': '0x00', 'range2': '0x01', 'decode': '2'}
    }

    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.settimeout (TIMEOUT)
        s.connect((ipAddress, int(port)))
        unitId = 01
        functionCode = 04

        for k, v in queryList.items():
            req = struct.pack('12B', 0x00, 0x00, 0x00, 0x00, 0x00, 0x06, int(unitId), int(functionCode), int(v['addr1'], 16), int(v['addr2'], 16), int(v['range1'], 16), int(v['range2'], 16))
            s.send(req)
            data = s.recv(BUFFER_SIZE)

            if v['decode'] == '1':
                statusCodes[k] = GetDataBytes(' '.join(format(ord(x), 'x') for x in data))
            elif v['decode'] == '2':
                l = GetDataBytes(' '.join(format(ord(x), 'x') for x in data))
                statusCodes[k] = str(256*int(l[0], 16) + int(l[1], 16))
            else:
                a = ' '.join(format(ord(x), 'x') for x in data)
                b = a.split()
                # Remove first 9 bytes, these are part of Modbus communication, start with first data char
                b = b[9:]
                # Remove NULL characters from the list
                while True:
                    try:
                        b.remove('0')
                    except ValueError:
                        break
                msg = ''
                for x in b:
                    msg += chr(int(str(x), 16))
                statusCodes[k] = str(msg.strip())
            
            time.sleep(0.05)
 
    except:
        result = {}
        result["operationStatus"] = "Offline"
        result["warningStatus"] = sys.exc_info()[1]
        result["typeName"] = ""
        result["inkName"] = ""
        result["inkOperatingTime"] = ""
        result["inkAlarmTime"] = ""
        result["printCount"] = ""
        result["cumulativeOperationTime"] = ""
        result["inkMakeupInkType"] = ""
        result["inkViscosity"] = ""
        result["inkPressure"] = ""
        result["ambientTemperature"] = ""
        result["deflectionVoltage"] = ""
        result["faultWarningMessageCount"] = ""
        return result

    finally:
        s.close()

    return CollectStatus(statusCodes)

def GetStatusTest(ipAddress, port):
    """Returns a mockup printer status. For testing purposes, when the printer is not accessible."""
    result = {}
    result["operationStatus"] = "Stop"
    result["warningStatus"] = "Makeup Ink Low Warning"
    result["typeName"] = "UX-D160W"
    result["inkName"] = "1072K"
    result["inkOperatingTime"] = "0"
    result["inkAlarmTime"] = "1200"
    result["printCount"] = "36"
    result["cumulativeOperationTime"] = "0"
    result["inkMakeupInkType"] = "18"
    result["inkViscosity"] = "0"
    result["inkPressure"] = "1"
    result["ambientTemperature"] = "23"
    result["deflectionVoltage"] = "23"
    result["faultWarningMessageCount"] = "23"
    return result
