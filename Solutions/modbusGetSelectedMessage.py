import socket
import struct
import sys
import time

def ToStr(listHex):
  fullString = ""
  for item in listHex:
    if len(item) == 1:
      item = '0' + item
    fullString = fullString + item
  return fullString.upper()

def GetSelMessName(ipAddress, port):

	BUFFER_SIZE = 1024
	TIMEOUT = 2.0
	statusCodes = {}
	data = ''

	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.settimeout (TIMEOUT)
	s.connect((ipAddress, int(port)))

	# req = struct.pack('15B', 0x00, 0x00, 0x00, 0x00, 0x00, 0x09, 0x01, 0x10, 0x00, 0x10, 0x00, 0x01, 0x01, 0x00, 0x00)
	s.send(chr(0x00) + chr(0x00) + chr(0x00) + chr(0x00) + chr(0x00) + chr(0x09) + chr(0x01) + chr(0x10) + chr(0x00) + chr(0x10) + chr(0x00) + chr(0x01) + chr(0x01) + chr(0x00) + chr(0x00))
	data = s.recv(BUFFER_SIZE)
	# req = struct.pack('12B', 0x00, 0x00, 0x00, 0x00, 0x00, 0x06, 0x01, 0x04, 0x0E, 0x42, 0x00, 0x0F)
	s.send(chr(0x00) + chr(0x00) + chr(0x00) + chr(0x00) + chr(0x00) + chr(0x06) + chr(0x01) + chr(0x04) + chr(0x0E) + chr(0x42) + chr(0x00) + chr(0x0F))
	data = s.recv(BUFFER_SIZE)

	selectedMessage = ' '.join(format(ord(x), 'x') for x in data)
	selectedMessage = ToStr(selectedMessage.split()[9:]).decode("hex").rstrip('\0').replace('\0','')

	return selectedMessage
