import socket
import sys

def GetDataBytes(response):
  return response.split()[2:]

def UpdateCommunicationStatus(status):
	if status == '30':
		result = 'Offline (Com=0)'
	elif status == '31':
		result = 'Online (Com=1)'
	else:
		result = status
	return result

def UpdateReceptionStatus(status):
	if status == '30':
		result = 'Reception not possible'
	elif status == '31':
		result = 'Reception possible'
	else:
		result = status
	return result

def UpdateStatusR(status):
	if UpdateStatusG(status) == '0':
		result = '1'
	else:
		result = '0'
	return result

def UpdateStatusG(status):
	if status == '32':
		result = '1'
	else:
		result = '0'
	return result

def UpdateOperationStatus(status):
	if status == '30':
		result = 'Stop'
	elif status == '31':
		result = 'Standby, Cover, Drop adjust, Ink heating'
	elif status == '32':
		result = 'Ready'
	elif status == '49':
		result = 'Stopping'
	elif status == '33':
		result = 'Deflection Voltage Fault'
	elif status == '34':
		result = 'Main Ink Tank Too Full'
	elif status == '35':
		result = 'Blank Print Items'
	elif status == '36':
		result = 'Ink Drop Charge Too Low'
	elif status == '37':
		result = 'Ink Drop Charge Too High'
	elif status == '38':
		result = 'Print Head Cover Open'
	elif status == '39':
		result = 'Target Sensor Fault'
	elif status == '3A':
		result = 'SYSTEM OPERATION ERROR C'
	elif status == '3B':
		result = 'Target Spacing Too Close'
	elif status == '3C':
		result = 'Improper Sensor Position'
	elif status == '3D':
		result = 'SYSTEM OPERATION ERROR M'
	elif status == '3E':
		result = 'Charge Voltage Fault'
	elif status == '3F':
		result = 'Barcode Short On Numbers'
	elif status == '41':
		result = 'Multi DC Power Supply Fan Fault'
	elif status == '42':
		result = 'Deflection Voltage Leakage'
	elif status == '43':
		result = 'Print Overlap Fault'
	elif status == '44':
		result = 'Ink Low Fault'
	elif status == '45':
		result = 'Makeup Ink Low Fault'
	elif status == '46':
		result = 'Print Data Changeover In Progress M'
	elif status == '47':
		result = 'Excessive Format Count'
	elif status == '48':
		result = 'Makeup Ink Replenishment Time-out'
	elif status == '4A':
		result = 'Ink Replenishment Time-out'
	elif status == '4B':
		result = 'No Ink Drop Charge'
	elif status == '4C':
		result = 'Ink Heating Unit Too High'
	elif status == '4D':
		result = 'Ink Heating Unit Temperature Sensor Fault'
	elif status == '4E':
		result = 'Ink Heating Unit Over Current'
	elif status == '4F':
		result = 'Internal Communication Error C'
	elif status == '50':
		result = 'Internal Communication Error M'
	elif status == '51':
		result = 'Internal Communication Error S'
	elif status == '52':
		result = 'SYSTEM OPERATION ERROR S'
	elif status == '53':
		result = 'Memory Fault C'
	elif status == '54':
		result = 'Memory Fault M'
	elif status == '55':
		result = 'Ambient Temperature Sensor Fault'
	elif status == '56':
		result = 'Print Controller Cooling Fan Fault'
	elif status == '59':
		result = 'Print Data Changeover In Progress S'
	elif status == '5A':
		result = 'Print Data Changeover In Progress V'
	elif status == '5D':
		result = 'Memory Fault S'
	elif status == '5E':
		result = 'Pump Motor Fault'
	elif status == '5F':
		result = 'Viscometer Ink Temperature Sensor Fault'
	elif status == '60':
		result = 'External Communication Error'
	elif status == '61':
		result = 'External Signal Error'
	elif status == '62':
		result = 'Memory Fault OP'
	elif status == '63':
		result = 'Ink Heating Unit Temperature Low'
	elif status == '64':
		result = 'Model-key Fault'
	elif status == '65':
		result = 'Language-key Fault'
	elif status == '66':
		result = 'Communication Buffer Fault'
	elif status == '67':
		result = 'Shutdown Fault'
	elif status == '68':
		result = 'Count Overflow'
	elif status == '69':
		result = 'Data changeover timing fault'
	elif status == '6A':
		result = 'Count changeover timing fault'
	elif status == '6B':
		result = 'Print start timing fault'
	elif status == '6C':
		result = 'Ink Shelf Life Information'
	elif status == '6D':
		result = 'Makeup Shelf Life Information'
	elif status == '71':
		result = 'Print Data Changeover Error C'
	elif status == '72':
		result = 'Print Data Changeover Error M'	
	else:
		result = status
	return result

def UpdateWarningStatus(status):
	if status == '30':
		result = 'No warning'
	elif status == '31':
		result = 'Ink Low Warning'
	elif status == '32':
		result = 'Makeup Ink Low Warning'
	elif status == '33':
		result = 'Ink Shelf Life Exceeded'
	elif status == '34':
		result = 'Battery Low M'
	elif status == '35':
		result = 'Ink Pressure High'
	elif status == '36':
		result = 'Product Speed Matching Error'
	elif status == '37':
		result = 'External Communication Error nnn'
	elif status == '38':
		result = 'Ambient Temperature Too High'
	elif status == '39':
		result = 'Ambient Temperature Too Low'
	elif status == '3B':
		result = 'External Signal Error nnn'
	elif status == '3C':
		result = 'Ink Pressure Low'
	elif status == '3D':
		result = 'Excitation V-ref. Review'
	elif status == '3E':
		result = 'Viscosity Reading Instability'
	elif status == '3F':
		result = 'Viscosity Readings Out of Range'
	elif status == '40':
		result = 'High Ink Viscosity'
	elif status == '41':
		result = 'Low Ink Viscosity'
	elif status == '42':
		result = 'Excitation V-ref. Review 2'
	elif status == '44':
		result = 'Battery Low C'
	elif status == '45':
		result = 'Calendar Content Inaccurate'
	elif status == '46':
		result = 'Excitation V-ref. Char. height Review'
	elif status == '49':
		result = 'Model-key Failure'
	elif status == '4A':
		result = 'Language-key Failure'
	elif status == '4C':
		result = 'Upgrade-key Fault'
	elif status == '50':
		result = 'Circulation System Cooling Fan Fault'
	elif status == '51':
		result = 'Ink Temperature Too High'
	else:
		result = status
	return result

def CollectStatus(printerData):
	data = GetDataBytes(printerData)
	result = {}
	print 'DEBUG'
	print printerData
	result["communicationStatus"] = UpdateCommunicationStatus(data[0])
	result["receptionStatus"] = UpdateReceptionStatus(data[1])
	result["operationStatus"] = UpdateOperationStatus(data[2])
	result["statusG"] = UpdateStatusG(data[2])
	result["statusR"] = UpdateStatusR(data[2])
	result["warningStatus"] = UpdateWarningStatus(data[3])
	return result

def GetStatus(ipAddress, port):
	BUFFER_SIZE = 1024
	result = {}
	data = ''

	try:
		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		s.settimeout (3.0)
		s.connect((ipAddress, int(port)))
		s.send(chr(0x1B) + chr(0x23))
		data = s.recv(BUFFER_SIZE)
		print data
		printerData = ' '.join(format(ord(x), 'x') for x in data)
	except:
		result["communicationStatus"] = ""
		result["receptionStatus"] = ""
		result["statusG"] = "0"
		result["statusR"] = "1"
		result["operationStatus"] = "Offline"
		result["warningStatus"] = sys.exc_info()[1]
		printerData = ""
		return result
	finally:
		s.close()

	return CollectStatus(printerData)

def GetStatusTest(ipAddress, port):
	printerData = "02 31 30 31 32 30 03"
	return CollectStatus(printerData)
