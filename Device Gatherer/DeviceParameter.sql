/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2016 (13.0.4001)
    Source Database Engine Edition : Microsoft SQL Server Express Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2016
    Target Database Engine Edition : Microsoft SQL Server Express Edition
    Target Database Engine Type : Standalone SQL Server
*/

USE [HitachiNew]
GO

/****** Object:  Table [dbo].[DeviceParameter]    Script Date: 08/05/2018 22:38:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DeviceParameter](
	[DeviceParameterID] [int] IDENTITY(1,1) NOT NULL,
	[DeviceName] [nvarchar](30) NULL,
	[DeviceIP] [nvarchar](30) NULL,
	[DevicePort] [int] NULL,
	[ParameterName] [nvarchar](100) NULL,
	[ParameterValue] [nvarchar](50) NULL,
	[DataRead] [datetime2](7) NULL,
 CONSTRAINT [PK_DeviceParameter] PRIMARY KEY CLUSTERED 
(
	[DeviceParameterID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

