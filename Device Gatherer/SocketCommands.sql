/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2016 (13.0.4001)
    Source Database Engine Edition : Microsoft SQL Server Express Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2016
    Target Database Engine Edition : Microsoft SQL Server Express Edition
    Target Database Engine Type : Standalone SQL Server
*/

USE [HitachiNew]
GO

/****** Object:  Table [dbo].[SocketCommands]    Script Date: 08/05/2018 22:39:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[SocketCommands](
	[SocketCommandsID] [int] IDENTITY(1,1) NOT NULL,
	[Command] [nvarchar](80) NULL,
	[TypeOfCommand] [int] NULL,
	[DeviceTypeID] [int] NULL,
	[ResultName] [nvarchar](40) NULL,
	[ParserID] [int] NULL,
	[HighAlert] [decimal](18, 2) NULL,
	[LowAlert] [decimal](18, 2) NULL,
 CONSTRAINT [PK_SocketCommands] PRIMARY KEY CLUSTERED 
(
	[SocketCommandsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[SocketCommands]  WITH CHECK ADD  CONSTRAINT [FK_SocketCommands_DeviceType] FOREIGN KEY([DeviceTypeID])
REFERENCES [dbo].[DeviceType] ([DeviceTypeID])
GO

ALTER TABLE [dbo].[SocketCommands] CHECK CONSTRAINT [FK_SocketCommands_DeviceType]
GO

