NiceLabel Printer Drivers
Windows Drivers for Thermal Printers by Euro Plus 

Windows Vista, Windows 7, Windows 8, Windows 8.1, Windows 10
Windows Server 2008, Windows Server 2008 R2,
Windows Server 2012, Windows Server 2012 R2


NiceLabel Printer Drivers make it possible to use your thermal printer in 
Windows environment as any standard Windows printer. Support for thermal 
printers is not natively included in Windows distributions and you have to 
obtain them elsewhere. Euro Plus is one of the leading thermal printer drivers 
developers. Apart from using TrueType fonts and graphics images with NiceLabel 
Printer Drivers, you can also print bar codes and use printer resident fonts for 
fastest printout.


OPTIMAL USE OF NICELABEL DRIVER VERS IS FROM LABELING SOFTWARE
--------------------------------------------------------------------

NiceLabel Printer Drivers allow you to print labels from any Windows application 
(MS Word, MS Excel, CorelDraw,...) but the real power of NiceLabel Printer 
Drivers is shown when you print labels from a professional Windows labeling 
software. NiceLabel is such a labeling package and it offers full label design 
control and easy connectivity to any external database containing label data. 
Here are more benefits of using NiceLabel Printer Drivers with NiceLabel 
package:

- Optimal usage of your printer capabilities with all built-in 
  functions (automatic serial numbering and bar code incrementing)
- Printing speed is pushed to the limit with usage of built-in 
  bar codes and optimisation of re-sending only the part of the label 
  that is different from previous one
- WYSIWYG designing and printing power of one of the most 
  technically advanced and user-friendly environments

NiceLabel is available in several editions, each created especially with end-
user in mind. Some packages are suitable for beginners and other for power users 
that need ultimate connectivity and integration with existing information 
systems.

For more information NiceLabel labeling software visit the Web site at 
http://www.nicelabel.com and download the trial version.


HOW TO INSTALL NiceLabel Printer Drivers
----------------------------------------------------------------------

The driver installation will begin when you click the Next button. On the next 
screen you will be able to select your printer model and finish the installation 
procedure. If you need to reinstall the printer driver at any later time or want 
to install a new model, please run the program PRNINST.EXE from the folder you 
extracted files to. Default location is C:\NiceLabel Printer Drivers\<your 
printer brand-name>


If for some reason you want to install printer drivers manually, or for the USB-
connected printer, see the Knowledge Base article KB177: Installing NiceLabel 
Printer Drivers available at

http://kb.nicelabel.com/index.php?t=faq&id=177

NiceLabel Printer Drivers are under constant development and modifications. New 
and updated drivers are available for free download from NiceLabel website:

http://www.nicelabel.com/downloads/printer-drivers


HOW TO CONTACT TECHNICAL SUPPORT
----------------------------------------------------------------------

If you have troubles using NiceLabel Printer Drivers see the following 
resources:

  Forums: http://forums.nicelabel.com
  Knowledge Base: http://www.nicelabel.com/Support/Knowledge-base
  NiceLabel Technical Support: http://www.nicelabel.com/Support/Technical-Support

Note: NiceLabel Printer Drivers are supported when used from NiceLabel software 
only. When you use NiceLabel Printer Drivers from some other labeling software, 
they are not subject of the support.

Note: NiceLabel printer drivers are no longer tested on Windows XP and Windows Server 2003.

Note: NiceLabel Printer Drivers version 4.2.00 and more have no support for the 
following operating systems: Windows 95 and 98, Windows Me, Windows NT. If you 
want to use NiceLabel Driver for these older Windows operating systems, download 
the matching older NiceLabel Printer Drivers (version before 4.2.00).


